.
├── README.md
├── content_long_list.txt
├── content_summary.txt
├── dataset
│   └── placeholder.txt
├── dep
│   └── placeholder.txt
├── documentation
│   ├── placeholder.txt
│   ├── robots.txt
│   └── scorecard
│       └── placeholder.txt
├── example
│   └── placeholder.txt
├── res
│   └── placeholder.txt
├── robots.txt
├── security.txt
├── src
│   └── placeholder.txt
├── structure.txt
├── test
│   └── placeholder.txt
├── tools
│   └── placeholder.txt
├── v1.x
│   ├── SDM-Methode_V1.0.pdf
│   ├── SDM-Methode_V1.1.pdf
│   └── SDM-Methodology_V1.0_English.pdf
└── v2.x
    ├── SDM-Methode_V2.0a.pdf
    ├── SDM-Methode_V2.0b.pdf
    └── SDM-Methodology_V2.0b_English.pdf

12 directories, 22 files
